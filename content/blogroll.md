---
title: Blogroll
author: Sebastian Schweer
type: page
slug: blogroll
categories:
  - blog
date: '2018-04-15'
---

Whenever I do find the time, I like to check out these blogs:

- [Wirres.net](https://wirres.net/) 
- [Linus Neumann](https://linus-neumann.de/)
- [Fefes Blog](https://blog.fefe.de/)
- [R Bloggers](https://www.r-bloggers.com/)
