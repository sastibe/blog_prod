---
title: "Don't forget to sharpen your axe"
publishDate: 2022-07-24T00:00:00+02:00
draft: false
thumbnailImagePosition: "bottom"
metaAlignment: "center"
categories: ["emacs"]
tags: ["org-mode"]
---

## A wise man once said... {#a-wise-man-once-said-dot-dot-dot}

> Give me six hours to chop down a tree and I will spend the first four sharpening the axe.
>
> -   Abraham Lincoln

In this statement there is a lot of truth. For instance, it is closely mimicked by on of [one of the 7 habits of highly effective people by Stephen Covey](https://genius.com/Stephen-r-covey-habit-7-sharpen-the-saw-annotated). Obviously, it applies to writing software as well, as a sharpened "developer's axe" means:

-   a suitable editor for writing and manipulating code, preferably with a good git integration,
-   good access to data if needed for analysis,
-   easy processes for deployment or publishing of the results with maximal automisation.


## The rule of three {#the-rule-of-three}

All of these aspects of the developer's axe need to be sharpened regularly. There looms a problem of overdoing it, however: how much time should be spent on sharpening vs. actual hacking? If Lincoln had said that he'd spend 5 hours on sharpening, that would be a bit too much, no?

Similarly, when optimizing one's own processes, one sometimes tends to overdo optimization and ends up getting nothing done. But at what point comes the sweet spot?

Though I cannot find the source for this any more, I recall reading a quote by [Hadley Wickham](https://en.wikipedia.org/wiki/Hadley_Wickham) somewhere that
"Before I do something manually the fourth time, I write a function for it". I've kept this rule in mind ever since, and I find it very well applicable in almost all situations: Three times are fine, but then you should consider functions/automization/....


## My own ungrinded axe {#my-own-ungrinded-axe}

Why am I writing all this? Because I was, until recently, working with a blunt axe myself and only now came to sharpening it:

I use [Hugo](https://gohugo.io/) to present my blog, but I write it in emacs using org mode. Hugo is able to ingest emacs org files directly, but there is also the possibility of exporting from org to Hugo via [ox-hugo](https://ox-hugo.scripter.co/). Up until the penultimate post, I used the former solution, due to the following reasons:

-   it worked somehow, even though I needed to  write an org file with some yaml at the beginning for the more fidgety parts of the header, I.e. thumbnail pic etc, and some html ALL IN THE SAME FILE...
-   there was no need for an export step from org to markdown.

In between these posts, I decided to sharpen my blog axe, and switch to a single, seperate org file that creates all of my future blog posts via ox-hugo. This lead to the following improvements:

-   much simpler writing of posts so far, because I only have to remember one set of Markdown Syntaxes and ox-hugo is incredibly well documented,
-   easy tracking of open tasks `org-agenda`, since the whole blog can be declared an `org-agenda-files`, and all open (i.e. "draft") articles show up as TODOs,
-   I can write this post on my smartphone, as the central org file for the blog is now separate, and lives in my Dropbox, synced to orgzly on my phone (only the export files reside in my git repo, syncing to the productive environment).

What has sharpening the axe done for me? Previously, I could only reasonably work on my laptop, unless I opted for copy pasting text from email into org files or something. At the moment of writing this post I'm enjoying the sun at the public pool, so ... an obvious plus.

The single org file used to built the last post and this one can be found [here](https://gitlab.com/sastibe/blog_org/-/blob/main/sastibe.org).

<img src="https://vg09.met.vgwort.de/na/0a2d6a1b4fc34a8a9fc54bf0adcd0b5c" width="1" height="1" alt="">