---
title: "A toy example for Reinforcement Learning, CLI via fire, CI/CD on gitlab in Python"
publishDate: 2022-07-25T00:00:00+02:00
draft: false
thumbnailImagePosition: "right"
metaAlignment: "center"
coverImage: "https://res.cloudinary.com/dlprdrxib/image/upload/w_1000,ar_16:9,c_fill,g_auto,e_sharpen/v1658869513/Screenshot_Neckar_tluxct.png"
thumbnailImage: "https://res.cloudinary.com/dlprdrxib/image/upload/w_200,ar_16:9,c_fill,g_auto,e_sharpen/v1658869513/Screenshot_Neckar_tluxct.png"
categories: ["data-science"]
tags: ["python", "tutorials"]
---

## Eager to Play TicTacToe? {#eager-to-play-tictactoe}

The package `neckar` contains a fully functional reinformcement learning model to train yourself and play against in the game TicTacToe. Here's how you can simply download and install the result if you have a gitlab account (and can set up a personal access token...):

```nil
pip install neckar --extra-index-url https://__token__:personalaccesstoken@gitlab.com/api/v4/projects/37714906/packages/pypi/simple
```

... and if you don't: Download the wheel from [this link](https://gitlab.com/sastibe/neckar/-/package_files/47214320/download), cd to the wheel and run

```nil
pip install neckar-0.1.1-py3-none-any.whl
```

In both cases, you then start plaxing against the trained model by runnning

```nil
reilingen tictactoe play
```

The standard model was trained in 20000 rounds before shipping. If the computer is too good for you, you can dumb it down like so:

```nil
reilingen tictactoe train --rounds=500
```

and you should be able to win a game, even though the computer always starts.


## How it's done: Reinforcement Learning {#how-it-s-done-reinforcement-learning}

This main part of the code, residing in `scr/neckar/tictactoe.py` is essentially copied directly from [here](https://towardsdatascience.com/reinforcement-learning-implement-tictactoe-189582bea542).
The only two major changes I did was in the data structure used: data frames instead of numpy arrays. This is to support the second change: the winning condition is evaluated with  regexes rather than with sums. I plan on extending the package to include connect four, and wanted to streamline the detection of winning patterns.

The mathematical background to the reinforcement learning part is temporal difference learning as described [on the wiki](https://en.m.wikipedia.org/wiki/Temporal_difference_learning). The main formula consists in updating the state probabilities after each run using the regression
![](https://res.cloudinary.com/dlprdrxib/image/upload/v1658869513/Screenshot_Equation_njcpty.png)
In the code, this line reads

```python
self.states_value[st] += self.lr * (self.decay_gamma * reward - self.states_value[st])
```

and its corresponding method takes up just five lines of code of the ~300 lines.


## How it's done: Package set up {#how-it-s-done-package-set-up}

As can be seen from the README, the package was set up with [PyScaffold](https://pypi.org/project/PyScaffold/). In order to start with a fresh scaffold, I simply ran

```nil
pip install pyscaffold
putup neckar
```

and receieved a working a skeleton of a Python package, with the right structure and some dummy files. From there, I simple replaced the reinforcement learning code at the right places and added sensible unit tests in `./tests` for my new winning detection.


## How it's done: CLI with fire {#how-it-s-done-cli-with-fire}

The skeleton provided by PyScaffold already comes with an entry point, see `setup.cfg`

```nil
[options.entry_points]
console_scripts =
     reilingen = neckar.main:run
```

I use `fire` to expose the functions `play` and `train` to the CLI. This allows the user to interact with the application and even train it, and is implemented in `main.py` and allows for the usage as described in [Eager to Play TicTacToe?](#eager-to-play-tictactoe)


## How it's done: CI/CD in gitlab {#how-it-s-done-ci-cd-in-gitlab}

The step from a PyScaffold setup to a CI/CD pipeline in gitlab is simple but has many, many positive consequences. The pipeline is defined in `.gitlab-ci.yml`, and it is run any time there is a new commit or a new tag in the package in gitlab. The step `run` contains the line

```nil
python -m build
python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
```

and therefore an entire whl is built any time a new commit is created. The final packages lie in the package repository and are publicly available as shown in Section [Eager to Play TicTacToe?](#eager-to-play-tictactoe)

<img src="https://vg09.met.vgwort.de/na/5686a95970fc4104b002e2641523d0d2" width="1" height="1" alt="">