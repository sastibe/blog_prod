---
title: "Writing DIN-Norm (German) Letters in Emacs"
publishDate: 2022-07-16T00:00:00+02:00
draft: false
thumbnailImagePosition: "right"
metaAlignment: "center"
thumbnailImage: "https://res.cloudinary.com/dlprdrxib/image/upload/c_thumb,w_200,g_face/v1658055625/emacs_letter_closed_dnlplr.jpg"
coverImage: "https://res.cloudinary.com/dlprdrxib/image/upload/c_scale,w_1200/v1658065087/emacs_letter_closed_dnlplr.jpg"
categories: ["emacs"]
tags: ["tutorials"]
---

## TL;DR {#tl-dr}

This post explains how to set up **emacs** so that the writing and sending of letters is always just a whim away. The resulting letters will adhere to the German [DIN 5008](https://de.wikipedia.org/wiki/DIN_5008) standard, and fit neatly into envelopes designed on that standard, as shown in the cover image on the top.


## Prerequisites {#prerequisites}

All you for the implementation shown in this tutorial is

-   Emacs with `org-mode` and
-   a LaTeX installation with the [dinbrief.cls](https://www.ctan.org/tex-archive/macros/latex/contrib/dinbrief) installed. On Ubuntu and Debian all you need are the packages `texlive-latex-base` and `texlive-latex-extra`.


## Setting up the capture template {#setting-up-the-capture-template}

I use the following set-up for capturing templates. It writes each new captured letter to a single "letter"-File in LETTERFILE.org. The process of starting a letter via capture, i.e. `C-c c l` then prompts you for filename/heading of the letter in the LETTERFILE, address (each line separately), topic and greeting formula. After that, you can simply write your plain text letter within the capture and finish with `C-c C-c`.

```lisp
(setq org-capture-templates '(("l" "Letter" entry
			     (file+headline "LETTERFILE.org" "Letters")
			     "* %<%F> %^{Filename}
:PROPERTIES:
:EXPORT_FILE_NAME: %<%F>_%\\1
:END:
#+OPTIONS: toc:nil
#+LaTeX_CLASS: dinbrief
#+LaTeX: \\begin{letter}{
#+LaTeX: %^{Adresszeile 1 [Name]} \\\\
#+LaTex: %^{Adresszeile 2 [Street, Number]} \\\\
#+LaTex: %^{Adresszeile 3 [Place, ZIP]} }
#+LaTeX: \\subject{%^{Topic}}
#+LaTeX: \\opening{%^{Greeting}}
%?
#+LaTeX: \\closing{Kind regards,}
#+LaTeX: \\underline{\\hspace{12cm}}\\\\
#+LaTeX: Sebastian  Schweer\\\\[2cm]
#+LaTeX: \\end{letter}" :jump-to-captured t)))
```


## Hints for further customization {#hints-for-further-customization}

Within the variable \`org-latex-classes\`, you can set up your backadress and favourite pacakges to use while writing, so you don't have to include these manually each time you write a letter. Obviously though, these settings should only be used for stationary information. Everything that might change from letter to letter should be handled within the capture template.

```lisp
(custom-set-variables
'(org-latex-classes
   '(("dinbrief" "\\documentclass{dinbrief}
\\address{Paul McCartney \\\\
Abbey Road 24 \\\\
6058 London}
\\backaddress{Paul McCartney, London}
\\place{London}"
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
      ("\\paragraph{%s}" . "\\paragraph*{%s}")
      ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
     ("article" "\\documentclass[11pt]{article}"
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
      ("\\paragraph{%s}" . "\\paragraph*{%s}")
      ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
     ("report" "\\documentclass[11pt]{report}"
      ("\\part{%s}" . "\\part*{%s}")
      ("\\chapter{%s}" . "\\chapter*{%s}")
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
     ("book" "\\documentclass[11pt]{book}"
      ("\\part{%s}" . "\\part*{%s}")
      ("\\chapter{%s}" . "\\chapter*{%s}")
      ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))
 '(org-latex-default-packages-alist
   '(("AUTO" "inputenc" t
      ("pdflatex"))
     ("T1" "fontenc" t
      ("pdflatex"))
     ("" "graphicx" t nil)
     ("" "grffile" t nil)
     ("" "longtable" nil nil)
     ("" "wrapfig" nil nil)
     ("" "rotating" nil nil)
     ("normalem" "ulem" t nil)
     ("" "amsmath" t nil)
     ("" "textcomp" t nil)
     ("" "amssymb" t nil)
     ("" "capt-of" nil nil)
     ("" "hyperref" nil nil)
     ("official" "eurosym" nil nil)
     ("" "ngerman" nil nil)))
 '(org-latex-title-command "")
```


## Usage {#usage}

The final step is simple ... just use it :) Capture your letter, answer all the prompts, and write your text. Once your capture is finished, navigate to the corresponding subtree in LETTERFILE, use and the `org-export` to latex to create your pdf: `C-c C-e C-s l o`.

{{< figure src="https://res.cloudinary.com/dlprdrxib/image/upload/c_scale,w_876/v1658055763/emacs_letter_open_pcdrkp.jpg" >}}

<img src="https://vg09.met.vgwort.de/na/01c2b5c8b67541e29f41bb33ccad4d68]]" width="1" height="1" alt="">