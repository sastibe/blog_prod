---
title: QueueingNetworkR - R Package
author: Sebastian Schweer
type: page
slug: queueingnetworkr
categories:
  - r
date: '2018-10-03'
---

<!-- Add icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

The newest version of the R package *QueueuingNetworkR*, written for the simulation and estimation in the article "Nonparametric Estimation of the Service Time Distribution in  Discrete-Time Queueing Networks", written together with Dr. Cornelia Wichelhaus, can be found here.

<form method="get" action="/queueingnetworkR_0.1.1.tar.gz">
   <button class="btn"><i class="fa fa-download"></i> Download source code</button>
</form>