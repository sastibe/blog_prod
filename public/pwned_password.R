library(httr)
library(digest)
library(ggmap)
library(plyr)
library(dplyr)
library(stringr)

popularity <- function(password){
  passw_hash = digest(password, "sha1", serialize = FALSE)
  passw_front = toupper(substr(passw_hash, 1, 5))
  passw_back = toupper(substr(passw_hash, 6, 200))
  hashes = 
    read.table(text = content(GET(paste0("https://api.pwnedpasswords.com/range/", 
                                         passw_front)),
                              encoding = "UTF-8"),
               sep = ":") %>%
    rename(hashes = V1, count = V2) %>%
    filter(hashes == passw_back) %>%
    mutate(password = password) %>%
    select(password, count)
  if(nrow(hashes) == 0){
    hashes <- tibble("password" = password, "count" = 0)
  }
  return(hashes)
}

ldply(c("freiburg", "stuttgart", "karlsruhe", "heidelberg", "mannheim", "konstanz"), popularity)

ldply(c("FCB", "BVB", "SCF", "S04", "VFB", "HSV", "Hertha"), popularity)

password_count <- ldply(c("freiburg", "stuttgart", "karlsruhe", "heidelberg", "mannheim", "konstanz", "berlin", "london", "madrid", "chester"), popularity)

#ldply(paste0("sebastian", str_pad(0:99, 2, pad = "0")), popularity)
size.loactions <- c(229636, 632743, 311919, 160601, 307997, 84440, 3613495, 8787892, 2190327, 118200)
       
cbind(password_count, size.loactions) %>%
  mutate(city_popularity = count/size.loactions) %>%
  arrange(-city_popularity)


### plots

register_google(key="AIzaSyBxQ5krj0_k6ThczXmpbVJB-KK12vmEd0s")

df.locations <- tibble(location = c("Freiburg, Germany", "Stuttgart, Germany", "Karlsruhe, Germany", "Heidelberg, Germany", "Mannheim, Germany", "Konstanz, Germany"))
# GEOCODE
geo.locations <- geocode(df.locations$location)



# COMBINE DATA
df.plot_locations <- cbind(df.locations, geo.locations, password_count, size.loactions)


# USE WITH GGPLOT
map <- get_map("Tübingen, Germany", zoom = 8) 
 map %>% ggmap() +
  geom_point(data = df.plot_locations, aes(x = lon, y = lat, size = size.loactions/100), color = 'red') +
   geom_point(data = df.plot_locations, aes(x = lon, y = lat, size = count/10), color = 'black') 
